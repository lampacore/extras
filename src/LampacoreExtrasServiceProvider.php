<?php namespace Lampacore\Extras;

use Illuminate\Support\ServiceProvider;

class LampacoreExtrasServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     * @return void
     */
    public function boot()
    {
        //$this->package('lampacore\extras', 'extras', base_path('workbench/lampacore/positions/resources'));
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'extras');

    }

    /**
     * Register the service provider.
     * @return void
     */
    public function register()
    {

    }

    /**
     * Get the services provided by the provider.
     * @return array
     */
    public function provides()
    {
        return [ ];
    }

}
