<?php

namespace Lampacore\Extras;

/**
 * Class ExtrasTrait
 * @package Extras
 * @property array extras
 * @property string attributes
 */
trait ExtrasTrait
{

    protected $extrasCollection = [ ];

    public function setExtrasFromInput()
    {
        if (\Input::has('extras')) {
            $this->setExtras(\Input::get('extras'));
        }
    }

    public function setExtras(array $extras)
    {

        foreach ($extras as $k => $v) {
            $this->setExtra($k, $v);
        }
    }

    public function setExtra($key, $value)
    {
        $this->extrasCollection[ $key ] = $value;

        $this->extras = $this->extrasCollection;

    }

    public function getExtra($key)
    {

        if (isset($this->extras[ $key ])) {
            return $this->extras[ $key ];
        } else {
            return null;
        }
    }

    public function getExtras()
    {
        return $this->extras;
    }

    public function setExtrasAttribute(array $value)
    {
        $this->attributes['extras'] = json_encode($value, JSON_PRETTY_PRINT);
    }

    public function getExtrasAttribute($value)
    {
        return json_decode($value, true);
    }

}
