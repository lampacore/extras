@foreach((array) $object->getExtraFields() as $key => $field)


    <div class="form-group">
        <label for="{{$key}}">{{$field['name']}}</label>

        @if(isset($field['type']) && $field['type'] == 'select')

            {!! Form::select("extras[$key]", $field['values'], $object->getExtra($key), ['class' => 'form-control']) !!}

        @elseif(isset($field['type']) && $field['type'] == 'email')

            <input type="email" class="form-control" id="extra_{{$key}}" name="extras[{{$key}}]"
                   placeholder="{{$field['name']}}" value="{!!$object->getExtra($key)!!}">

        @elseif(isset($field['type']) && $field['type'] == 'date')

            <input type="date" class="form-control" id="extra_{{$key}}" name="extras[{{$key}}]"
                   placeholder="{{$field['name']}}" value="{!!$object->getExtra($key)!!}">

        @elseif(isset($field['type']) && $field['type'] == 'text')


            <textarea class="form-control" id="extra_{{$key}}" name="extras[{{$key}}]" placeholder="{{$field['name']}}"
                      rows="10">{{$object->getExtra($key)}}</textarea>


        @else

            <input type="text" class="form-control" id="extra_{{$key}}" name="extras[{{$key}}]"
                   placeholder="{{$field['name']}}" value="{{$object->getExtra($key)}}">

        @endif

    </div>

@endforeach